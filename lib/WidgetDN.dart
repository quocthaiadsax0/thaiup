import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';

import 'main.dart';

class WidgetDN extends StatefulWidget {
  @override
  _WidgetDNState createState() => _WidgetDNState();
}

class _WidgetDNState extends State<WidgetDN> {
  late WebViewController _controller;
  // ignore: unused_field
  double _progress = 0;
  Future<String>? initialUrlFuture;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    initialUrlFuture = checkUrls();
  }

  Future<String> checkUrls() async {
    List<String> urls = await fetchUrlsFromAPI();

    for (String url in urls) {
      if (await isWebsiteAccessible(url)) {
        return url;
      }
    }

    return 'https://shbetrx.com/';
  }

  Future<bool> isWebsiteAccessible(String url) async {
    try {
      final response = await http.get(Uri.parse(url));
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: initialUrlFuture,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.hasData) {
          return SafeArea(
            child: WebView(
              initialUrl: snapshot.data,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController controller) {
                _controller = controller;
              },
              onPageStarted: (String url) {
                setState(() {
                  _progress = 0;
                });
              },
              onPageFinished: (String url) {
                setState(() {
                  _progress = 100;
                });
                // ignore: deprecated_member_use
                _controller.evaluateJavascript(
                    "document.body.style.paddingBottom = '200px';");
              },
            ),
          );
        } else {
          return Center(
              child: Text('Đang kiểm tra',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.yellow,
                  )));
        }
      },
    );
  }
}
