import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'WidgetDN.dart';
import 'dart:convert';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => UrlProvider(),
      child: MyApp(),
    ),
  );
}

Future<List<String>> fetchUrlsFromAPI() async {
  final response =
      await http.get(Uri.parse('http://45.118.146.216/checkurl/check_url.php'));

  if (response.statusCode == 200) {
    List<dynamic> data = json.decode(response.body);
    List<String> urls = List<String>.from(data);
    return urls;
  } else {
    throw Exception('Failed to load URLs from API');
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'SHBET Nhà Cái Uy Tín',
        debugShowCheckedModeBanner: false,
        home: WidgetDN());
  }
}
class UrlProvider with ChangeNotifier {
  String? _workingUrl;

  String? get workingUrl => _workingUrl;

  set workingUrl(String? value) {
    _workingUrl = value;
    notifyListeners();
  }
}